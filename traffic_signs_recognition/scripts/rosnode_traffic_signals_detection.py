#!/usr/bin/env python
import os
import rospy
from sensor_msgs.msg import Image
import rospkg
from cv_bridge import CvBridge, CvBridgeError
import cv2
from PIL import Image as ImagePil
import numpy as np
from mobilenet_class import TrafficSignalsRecognition


class ROSTrafficLightRecogniser(object):

    def __init__(self, rgb_camera_topic, MODEL_NAME, plot_flag=True, imgs_not_process=4):
        rospy.loginfo("Start ROSTrafficLightRecogniser Init process...")
        self.imgs_not_process = imgs_not_process
        self.img_proc_count = 0
        self._MODEL_NAME = MODEL_NAME
        self._plot_flag = plot_flag

        self.current_img = None

        # get an instance of RosPack with the default search paths
        self.rate = rospy.Rate(5)
        rospack = rospkg.RosPack()
        # get the file path for traffic_light_recognition
        self.path_to_package = rospack.get_path('traffic_light_recognition')

        self.bridge_object = CvBridge()
        rospy.loginfo("Start camera suscriber...")
        self.cam_topic = rgb_camera_topic
        self._check_cam_ready()
        self.image_sub = rospy.Subscriber(self.cam_topic,Image,self.camera_callback)
        
        
        self.traffic_signals_rec_obj = TrafficSignalsRecognition()
        
        rospy.loginfo("Finished ROSTrafficLightRecogniser Init process...Ready")



    def _check_cam_ready(self):
      self.cam_image = None
      while self.cam_image is None and not rospy.is_shutdown():
         try:
               self.cam_image = rospy.wait_for_message(self.cam_topic, Image, timeout=1.0)
               rospy.logdebug("Current "+self.cam_topic+" READY=>" + str(self.cam_image))

         except:
               rospy.logerr("Current "+self.cam_topic+" not ready yet, retrying.")

    def camera_callback(self,data):
        self.cam_image = data

    def loop(self):

        while not rospy.is_shutdown():
            rospy.logdebug("In loop...")
            self.recognise(self.cam_image)
            
            if self.current_img is not None:
                img_rgb = cv2.cvtColor(self.current_img, cv2.COLOR_BGR2RGB)
                cv2.imshow('Traffic Light detections',img_rgb )
                cv2.waitKey(1)

            self.rate.sleep()

    def recognise(self,data):

        if self.img_proc_count >= self.imgs_not_process:
            self.img_proc_count = 0
            rospy.loginfo("Processing Image")
            # Get a reference to webcam #0 (the default one)
            try:
                # We select bgr8 because its the OpneCV encoding by default
                video_capture = self.bridge_object.imgmsg_to_cv2(data, desired_encoding="rgb8")
            except CvBridgeError as e:
                print(e)

            small_frame = cv2.resize(video_capture, (0, 0), fx=0.5, fy=0.5)

            out_commands = self.traffic_rec_obj.detect_traffic_lights(small_frame)
            print(out_commands)  # commands to print action type, for 'Go' this will return True and for 'Stop' this will return False

            self.current_img = self.traffic_rec_obj.get_detected_traficlight_img()
        else:
            self.img_proc_count += 1
            rospy.logwarn("Not Processing Image")



def main():
    
    MODEL_NAME = 'faster_rcnn_resnet101_coco_11_06_2017'  # for improved accuracy
    traffic_light_object = ROSTrafficLightRecogniser("/rgb_camera/image_raw",MODEL_NAME, False)

    traffic_light_object.loop()
    cv2.destroyAllWindows()

def test():
    # Specify number of images to detect
    Num_images = 17

    # Specify test directory path
    PATH_TO_TEST_IMAGES_DIR = './test_images'

    # Specify downloaded model name
    # MODEL_NAME ='ssd_mobilenet_v1_coco_11_06_2017'    # for faster detection but low accuracy
    MODEL_NAME = 'faster_rcnn_resnet101_coco_11_06_2017'  # for improved accuracy
    # --------test images------
    TEST_IMAGE_PATHS = [os.path.join(PATH_TO_TEST_IMAGES_DIR, 'img_{}.jpg'.format(i)) for i in range(1, Num_images + 1)]

    rgb_camera_topic="/camera/rgb/image_raw"

    obj = TrafficLightRecogniser(MODEL_NAME, True)
    for image_path in TEST_IMAGE_PATHS:
        image = ImagePil.open(image_path)

        # the array based representation of the image will be used later in order to prepare the
        # result image with boxes and labels on it.
        image_np = obj.load_image_into_numpy_array(image)

        print(type(image_np))

        out_commands = obj.detect_traffic_lights(image_np)
        print(out_commands)  # commands to print action type, for 'Go' this will return True and for 'Stop' this will return False

if __name__ == '__main__':
    rospy.init_node('traffic_signs_object_node', anonymous=True, log_level=rospy.DEBUG)
    main()